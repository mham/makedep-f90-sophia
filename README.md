# makedepf90
This is a copy of Erik Edelman's tool for generating make dependencies for
Fortran code.

As the source code does not appear to be available at the original site, I
have posted a copy here with minor improvements for the sake of package
managers and anyone who wants to add patches.

Option1: Do not use this git but simply do (only for Linux):
$ sudo apt-get install makedepf90

Option2: Use this git and do the below steps:

As stated in the readme.md the way to leverage parallel make is:

$ ./configure

$ make

$ make install

Notice, the very first time you make there will not be a ./configure file.
Thus, you will need to rely on autoconfigure and some other packages.
Therefore, make sure you have the following installed:

$ sudo apt-get install zlib1g-dev

$ sudo apt-get install libxmu-dev libxmu-headers freeglut3-dev libxext-dev libxi-dev

$ sudo apt-get install flex g++ automake autoconf make

$ sudo apt install -y file

$ sudo apt-get install bison

Notice also, that if you have had a few unsuccesful attemps to make
you may need to remove temporary files. Therefore, the first time
always do:

$ make clean

$ make realclean

$ make

$ make install

Then go to ./ellipsys3d/Executables and test the parallel make capabilities by making EllipSys3D.
